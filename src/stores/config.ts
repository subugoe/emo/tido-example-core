import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useConfigStore = defineStore('config', () => {
  const config = ref({
    panels: [
      { comp: 'TidoExampleModule' }
    ]
  })

  return { config }
})
